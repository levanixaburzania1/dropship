import "./main.css";
import Products from "./Products/Products";

const Main = ({
  searchQuery,
  setNumberOfItems,
  setCountMarked,
  toggleOnClickClear,
  toggleOnClickSelect,
  selectSortValue,
  category,
  minPrice,
  maxPrice,
}) => {
  return (
    <main id="main" className="main">
      <Products
        searchQuery={searchQuery}
        setNumberOfItems={setNumberOfItems}
        setCountMarked={setCountMarked}
        toggleOnClickClear={toggleOnClickClear}
        toggleOnClickSelect={toggleOnClickSelect}
        selectSortValue={selectSortValue}
        category={category}
        maxPrice={maxPrice}
        minPrice={minPrice}
      />
    </main>
  );
};

export default Main;
