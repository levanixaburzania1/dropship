import "./products.css";
import ProductHead from "./ProductHead";
import Modal from "./Modal/Modal";

import { useEffect, useState } from "react";

const SingleProduct = ({
  product,
  setCountMarked,
  toggleOnClickClear,
  toggleOnClickSelect,
}) => {
  const [openModal, setOpenModal] = useState(false);
  const showModal = () => {
    setOpenModal(true);
  };

  const setCloseModal = () => {
    setTimeout(() => setOpenModal(false), 100);
  };
const [onHeadBtn,setOnHeadBtn] = useState(false);
  return (
    <div className="product" onMouseOver={()=>setOnHeadBtn(true)} onMouseOut={()=>setOnHeadBtn(false)}> 
      <div className="product__img" onClick={showModal}>
        <img src={product.image} />
      </div>
      <h3 className="product__title">{product.title}</h3>
      <div className="product__category">
        By:{" "}
        <span className="product__category--highlited">{product.category}</span>
      </div>
      <div className="product__price">${product.price}</div>
      <ProductHead
        setCountMarked={setCountMarked}
        toggleOnClickClear={toggleOnClickClear}
        toggleOnClickSelect={toggleOnClickSelect}
        onHeadBtn={onHeadBtn}
      />
      {openModal && <Modal product={product} setCloseModal={setCloseModal} />}
    </div>
  );
};

export default SingleProduct;
