import "./products.css";
import Checkbox from "./Checkbox";
import Btn from "../../Btn/Btn";
import { useEffect, useState } from "react";

const ProductHead = ({
  setCountMarked,
  toggleOnClickClear,
  toggleOnClickSelect,
  onHeadBtn
}) => {

  const [showHadBtn,setShowHedBtn] = useState(false);
  useEffect(()=>{setShowHedBtn(onHeadBtn)},[onHeadBtn]);

  return (
    <div className="product__head">
      <Checkbox
        setCountMarked={setCountMarked}
        toggleOnClickClear={toggleOnClickClear}
        toggleOnClickSelect={toggleOnClickSelect}
      />
      {showHadBtn && <Btn content="Add to Inventory" />}
    </div>
  );
};

export default ProductHead;
