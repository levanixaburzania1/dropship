import Btn from "../../Btn/Btn";
import "./products.css";
import { useEffect, useState } from "react";
import SingleProduct from "./SingleProduct";
import GenProducts from "./GenProducts";

const Products = ({
  searchQuery,
  setNumberOfItems,
  setCountMarked,
  toggleOnClickClear,
  toggleOnClickSelect,
  selectSortValue,
  category,
  minPrice,
  maxPrice,
}) => {
  const [productsData, setProductsData] = useState(null);

  const getProductsData = async () => {
    const request = await fetch("https://fakestoreapi.com/products");
    return request.json();
  };

  useEffect(() => {
    getProductsData().then((response) => setProductsData(response));
  }, []);

  return (
    <GenProducts
      products={productsData}
      searchQuery={searchQuery}
      setNumberOfItems={setNumberOfItems}
      setCountMarked={setCountMarked}
      toggleOnClickClear={toggleOnClickClear}
      toggleOnClickSelect={toggleOnClickSelect}
      selectSortValue={selectSortValue}
      category={category}
      maxPrice={maxPrice}
      minPrice={minPrice}
    />
  );
};

export default Products;
