import "./modal.css";
import Btn from "../../../Btn/Btn";
const Modal = ({ product, setCloseModal }) => {
  return (
    <div className="modal">
      <div className="modal__content">
        <div className="modal__left-side">
          <div className="modal__price">COST: ${product.price}</div>
          <div className="modal__photo">
            <img src={product.image} />
          </div>
        </div>
        <div className="modal__right-side">
          <h3 className="modal__title">{product.title}</h3>
          <div className="modal__btn">
            <Btn content="ADD TO INVENTORY" />
          </div>
          <h4 className="modal__description-title">description</h4>
          <hr />
          <p className="modal__description">{product.description}</p>
        </div>
        <div className="close-modal" onClick={setCloseModal}>
          <i class="fa fa-times" aria-hidden="true"></i>
        </div>
      </div>
    </div>
  );
};

export default Modal;
