import "./products.css";
import { useEffect, useState } from "react";
const Checkbox = ({
  setCountMarked,
  toggleOnClickClear,
  toggleOnClickSelect,
}) => {
  const [isChecked, setIsChecked] = useState(false);

  const checkboxChanged = (e) => {
    const checked = e.target.checked ? 1 : -1;
    setCountMarked(checked);

    setIsChecked(isChecked ? false : true);
  };

  // cleaer all function

  useEffect(() => {
    setIsChecked(false);
  }, [toggleOnClickClear]);

  // select all

  const [markAll, setMarkAll] = useState(false);
  useEffect(() => {
    setIsChecked(markAll);
    setMarkAll(true);
  }, [toggleOnClickSelect]);

  return (
    //     <div className="checkbox-input">
    //     <div class="container">
    // <div class="round">
    <input
      type="checkbox"
      id="checkbox"
      onChange={checkboxChanged}
      checked={isChecked}
    />

    //     <label for="checkbox"></label>
    // </div>
    // </div>
    //     </div>
  );
};

export default Checkbox;
