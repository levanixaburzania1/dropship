import "./products.css";
import SingleProduct from "./SingleProduct";
import { useState, useEffect } from "react";

const GenProducts = ({
  products,
  searchQuery,
  setNumberOfItems,
  setCountMarked,
  toggleOnClickClear,
  toggleOnClickSelect,
  selectSortValue,
  category,
  minPrice,
  maxPrice,
}) => {
  // quantity of all items
  setNumberOfItems(
    products &&
      products.filter(
        (product) =>
          product.title.toUpperCase().includes(searchQuery.toUpperCase()) &&
          product.price >= minPrice &&
          product.price <= maxPrice &&
          (!category || product.category == category)
      ).length
  );

  if (!selectSortValue) {
    return (
      <div id="main-content">
        {products &&
          products
            .filter(
              (product) =>
                product.title
                  .toUpperCase()
                  .includes(searchQuery.toUpperCase()) &&
                product.price >= minPrice &&
                product.price <= maxPrice &&
                (!category || product.category == category)
            )
            .map((product) => (
              <SingleProduct
                product={product}
                key={product.id}
                setCountMarked={setCountMarked}
                toggleOnClickClear={toggleOnClickClear}
                toggleOnClickSelect={toggleOnClickSelect}
              />
            ))}
      </div>
    );
  } else if (selectSortValue == "asc-name") {
    return (
      <div id="main-content">
        {products &&
          products
            .filter(
              (product) =>
                product.title
                  .toUpperCase()
                  .includes(searchQuery.toUpperCase()) &&
                product.price >= minPrice &&
                product.price <= maxPrice &&
                (!category || product.category == category)
            )
            .sort((a, b) => (a.title < b.title ? 1 : -1))
            .map((product) => (
              <SingleProduct
                product={product}
                key={product.id}
                setCountMarked={setCountMarked}
                toggleOnClickClear={toggleOnClickClear}
                toggleOnClickSelect={toggleOnClickSelect}
              />
            ))}
      </div>
    );
  } else if (selectSortValue == "desc-name") {
    return (
      <div id="main-content">
        {products &&
          products
            .filter(
              (product) =>
                product.title
                  .toUpperCase()
                  .includes(searchQuery.toUpperCase()) &&
                product.price >= minPrice &&
                product.price <= maxPrice &&
                (!category || product.category == category)
            )
            .sort((a, b) => (a.title < b.title ? -1 : 1))
            .map((product) => (
              <SingleProduct
                product={product}
                key={product.id}
                setCountMarked={setCountMarked}
                toggleOnClickClear={toggleOnClickClear}
                toggleOnClickSelect={toggleOnClickSelect}
              />
            ))}
      </div>
    );
  } else if (selectSortValue == "asc-price") {
    return (
      <div id="main-content">
        {products &&
          products
            .filter(
              (product) =>
                product.title
                  .toUpperCase()
                  .includes(searchQuery.toUpperCase()) &&
                product.price >= minPrice &&
                product.price <= maxPrice &&
                (!category || product.category == category)
            )
            .sort((a, b) => a.price - b.price)
            .map((product) => (
              <SingleProduct
                product={product}
                key={product.id}
                setCountMarked={setCountMarked}
                toggleOnClickClear={toggleOnClickClear}
                toggleOnClickSelect={toggleOnClickSelect}
              />
            ))}
      </div>
    );
  } else if (selectSortValue == "desc-price") {
    return (
      <div id="main-content">
        {products &&
          products
            .filter(
              (product) =>
                product.title
                  .toUpperCase()
                  .includes(searchQuery.toUpperCase()) &&
                product.price >= minPrice &&
                product.price <= maxPrice &&
                (!category || product.category == category)
            )
            .sort((a, b) => b.price - a.price)
            .map((product) => (
              <SingleProduct
                product={product}
                key={product.id}
                setCountMarked={setCountMarked}
                toggleOnClickClear={toggleOnClickClear}
                toggleOnClickSelect={toggleOnClickSelect}
              />
            ))}
      </div>
    );
  }
};

export default GenProducts;
