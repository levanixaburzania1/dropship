import "./btn.css";

const Btn = ({ content, toggleClearBtn, toggleSelectBtn }) => {
  const action = (e) => {
    if (e.target.value == "CLEAR") {
      toggleClearBtn();
    } else if (e.target.value == "SELECT ALL") {
      toggleSelectBtn();
    }
  };

  return (
    <button className="btn" onClick={action} value={content}>
      {content}
    </button>
  );
};

export default Btn;
