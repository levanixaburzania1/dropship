import { useState, useEffect } from "react";
import "./App.css";

import Header from "./Header/Header";
import Sidebar from "./Sidebar/Sidebar";
import MainNav from "./MainNav/MainNav";
import Main from "./Main/Main";

function App() {
  // search
  const [searchQuery, setSearcquery] = useState("");

  const findeProduct = (query) => {
    setSearcquery(query);
  };

  // marked and sum of all items

  const [allItems, setAllItems] = useState(0);
  const setNumberOfItems = (items) => {
    setAllItems(items);
  };

  const [markedItems, setMarkedItems] = useState(0);
  const setCountMarked = (increment) => {
    setMarkedItems(markedItems + increment);
  };
  // clear all
  const [toggleOnClickClear, setToggleOnClickClear] = useState(false);
  const toggleClear = () => {
    setToggleOnClickClear(toggleOnClickClear ? false : true);
    setMarkedItems(0);
  };

  // select all

  const [toggleOnClickSelect, setToggleOnClickSelect] = useState(false);
  const onToggleOnClickSelect = () => {
    setToggleOnClickSelect(toggleOnClickSelect ? false : true);
    setMarkedItems(allItems);
  };

  // sort

  const [selectSortValue, setSelectSortValue] = useState(null);
  const pickSelectValue = (value) => {
    setSelectSortValue(value);
  };
  console.log(selectSortValue);
  // category
  const [category, setCategory] = useState(null);
  const changeCategory = (cat) => {
    setCategory(cat);
  };

  // price range
  const [maxPrice, setMaxPrice] = useState(1000);
  const [minPrice, setMinPrice] = useState(0);

  const setPriceRange = (max, min) => {
    setMinPrice(min);
    setMaxPrice(max);
  };

  return (
    <div className="App">
      <div className="content">
        <Header
          findProduct={findeProduct}
          allItems={allItems}
          markedItems={markedItems}
          markedProducts={markedItems}
          toggleClearBtn={toggleClear}
          toggleSelectBtn={onToggleOnClickSelect}
          setSelectSortValue={setSelectSortValue}
        />
        <Sidebar
          changeCategory={changeCategory}
          setPriceRange={setPriceRange}
        />
        <MainNav />
        <Main
          searchQuery={searchQuery}
          setNumberOfItems={setNumberOfItems}
          setCountMarked={setCountMarked}
          toggleOnClickClear={toggleOnClickClear}
          toggleOnClickSelect={toggleOnClickSelect}
          selectSortValue={selectSortValue}
          category={category}
          maxPrice={maxPrice}
          minPrice={minPrice}
        />
      </div>
    </div>
  );
}

export default App;
