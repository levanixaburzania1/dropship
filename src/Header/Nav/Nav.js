import "./nav.css";
import { useState } from "react";

const Nav = ({ findProduct }) => {
  const [searchInput, setSearchInput] = useState("");

  const onSearch = () => {
    findProduct(searchInput);
  };
  return (
    <div className="nav">
      <input
        className="nav__search-input"
        type="text"
        placeholder="search..."
        onChange={(e) => {
          setSearchInput(e.target.value);
        }}
      />
      <button className="nav__search-btn" onClick={onSearch}>
        <i className="fa fa-search" aria-hidden="true"></i>
      </button>
    </div>
  );
};

export default Nav;
