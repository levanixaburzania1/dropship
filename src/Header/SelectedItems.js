const SelectedItems = ({ allItems, markedItems }) => {
  return (
    <span className="header__selected-items">
      <span className="header__selected-items--hidden">
        selected <span className="selected-items__quantity">{markedItems}</span>{" "}
        out of
      </span>{" "}
      {allItems} products
    </span>
  );
};

export default SelectedItems;
