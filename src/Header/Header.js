import "./header.css";
import Nav from "./Nav/Nav";
import Sort from "./Sort/Sort";
import Btn from "../Btn/Btn";
import SelectedItems from "./SelectedItems";

const Header = ({
  findProduct,
  allItems,
  markedItems,
  markedProducts,
  toggleClearBtn,
  toggleSelectBtn,
  setSelectSortValue,
}) => {
  return (
    <header className="header">
      <div className="header__raw1">
        <div className="header__left">
          <Btn content="SELECT ALL" toggleSelectBtn={toggleSelectBtn} />
          <span className="seperator"></span>
          <SelectedItems allItems={allItems} markedItems={markedItems} />
          {markedProducts > 0 && (
            <Btn content="CLEAR" toggleClearBtn={toggleClearBtn} />
          )}
          {/* <Btn content="FILTER"/> */}
        </div>
        <div className="header__right">
          <Nav findProduct={findProduct} />
          <Btn content="ADD TO INVENTORY" />
          <button className="main-nav-bar">
            <i class="fa fa-bars" aria-hidden="true"></i>
          </button>
          <span className="header__help">
            <i className="fa fa-question" aria-hidden="true"></i>
          </span>
        </div>
      </div>
      <Sort setSelectSortValue={setSelectSortValue} />
    </header>
  );
};

export default Header;
