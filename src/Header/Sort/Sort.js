import "./sort.css";
import { TiArrowUnsorted } from "react-icons/ti";
import { MdSort } from "react-icons/md";
import {useState} from 'react';
const Sort = ({ setSelectSortValue }) => {

  const [currentSort,setCurrentSort] = useState("New Arrivals");
  const onSortOptionClick = (e) => {
    console.log(e.target.value);
    setCurrentSort(e.target.innerHTML);
  };

  const  [showSortOptions,setShowSortOptions] = useState(false);

  return (
    <div className="sort__select">
    <div className="sort" onClick={()=>{setShowSortOptions(showSortOptions ? false : true)}}>
      <div className="sort__head">
        {" "}
        <MdSort className="sort-logo" />
        Sort By: <span className="sort-type">{currentSort}</span>
        <TiArrowUnsorted />
      </div>
      {showSortOptions && (
        <div className="sort__options">
          <div className="sort__option" onClick={onSortOptionClick} value="asc-price">
            Price: Law To High
          </div>
          <div className="sort__option" onClick={onSortOptionClick} value="desc-price">
            Price: High To Law
          </div>
          <div className="sort__option" onClick={onSortOptionClick} value="asc-name">
            Name:A-Z
          </div>
          <div className="sort__option" onClick={onSortOptionClick} value="desc-name">
            Name:Z-A
          </div>
        </div>
      )}
    </div>
  </div>


  );
};

export default Sort;
