import "./select.css";

const Select = ({ title, color, optionsArr, changeCategory }) => {
  const options =
    optionsArr &&
    optionsArr.map((item) => <option value={item}>{item}</option>);
  const onCategoryChange = (e) => {
    changeCategory(e.target.value);
  };

  return (
    <select
      name=""
      id=""
      className={"sidebar__select " + color}
      onChange={onCategoryChange}
    >
      <option value="" hidden>
        {title}
      </option>
      {options}
    </select>
  );
};

export default Select;
