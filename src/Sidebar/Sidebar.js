import "./sidebar.css";
import Btn from "../Btn/Btn";
import Select from "./Select/Select";
import PriceRange from "./PriceRange/PriceRange";
import { useState } from "react";

const Sidebar = ({ changeCategory, setPriceRange }) => {
  const [category, setCategory] = useState([
    "electronics",
    "jewelery",
    "men's clothing",
    "women's clothing",
  ]);
  const [niche, setNiche] = useState([]);

  return (
    <div className="sidebar">
      <Select
        title="Choose Niche"
        color="sidebar__select--dark"
        optionsArr={niche}
      />
      <Select
        title="Choose Category"
        color="sidebar__select--light"
        optionsArr={category}
        changeCategory={changeCategory}
      />
      <PriceRange setPriceRange={setPriceRange} />

      <Btn content="RESET FILTER" />
    </div>
  );
};

export default Sidebar;
