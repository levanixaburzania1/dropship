import "./priceRange.css";
import { useState, useEffect } from "react";

const PriceRange = ({ setPriceRange }) => {
  const [value1, setValue1] = useState(0);
  const [value2, setValue2] = useState(1000);

  const changeValue1 = (e) => {
    setValue1(e.target.value);
  };

  const changeValue2 = (e) => {
    setValue2(e.target.value);
  };

  useEffect(() => {
    setPriceRange(Math.max(value1, value2), Math.min(value1, value2));
  }, [changeValue2, changeValue2]);
  return (
    <div className="price">
      <div className="rangeslider">
        <input
          className="min"
          name="range_1"
          type="range"
          min="0"
          max="1000"
          value={value1}
          onChange={changeValue1}
        />
        <input
          className="max"
          name="range_1"
          type="range"
          min="0"
          max="1000"
          value={value2}
          onChange={changeValue2}
        />
        <span className="range_min light left">
          $ {Math.min(value1, value2)}
        </span>
        <span className="range_max light right">
          $ {Math.max(value1, value2)}
        </span>
      </div>
    </div>
  );
};

export default PriceRange;
