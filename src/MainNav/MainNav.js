import './mainNav.css'

const MainNav = () => {
    return(
        <div className="main-nav">
        <img src="/images/Logo-365dropship.jpg" /> 
       <div class="main-nav__btn"> <i class="fa fa-user-circle-o" aria-hidden="true"></i></div>
       <div class="main-nav__btn"> <i class="fa fa-tachometer" aria-hidden="true"></i></div>
       <div class="main-nav__btn main-nav__btn--selected"> <i class="fa fa-list fa--hilighted" aria-hidden="true"></i></div>
       <div class="main-nav__btn"> <i class="fa fa-cube" aria-hidden="true"></i></div>
       <div class="main-nav__btn"> <i class="fa fa-shopping-cart" aria-hidden="true"></i></div>
       <div class="main-nav__btn"> <i class="fa fa-check-square" aria-hidden="true"></i></div>
       <div class="main-nav__btn"> <i class="fa fa-exchange" aria-hidden="true"></i></div>
       <div class="main-nav__btn"> <i class="fa fa-list-alt" aria-hidden="true"></i></div>


      </div>
    )
}

export default MainNav;